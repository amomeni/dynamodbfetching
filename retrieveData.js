// using aws sdk
var AWS = require('aws-sdk');
// setting region
AWS.config.update({region: 'us-west-2'});

// create dynamoDB service object
var ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

// requiring fs module in which readFile function is defined
const fs = require('fs');

// globally defining keysPayload
var keysPayload = []; 
  
 // reading in the file
fs.readFile('sas.txt', 'ascii', (err, data) => { 
    if (err) throw err; 
  
	console.log('-- raw file data');
    console.log(data);
	
	// splitting by new line & carriage returns 
    lines = data.split('\r\n');
	
	// checking content length
	console.log('lines ' + lines.length);
	
	// only a hundred at a time because that's an dynamoDb batchGetItem limitation
    for(var line = 0; line < 100; line++){
	  let cleanLine = lines[line].replace(/(\r\n|\n|\r)+/gm, "");
      console.log("clean line: " + cleanLine);
	  keysPayload.push({'Service Appointment': {S: cleanLine}});
    }

	// creating the payload for dynamoDb
	var params = {
	  RequestItems: {
		'release-form-production': {
		  Keys: keysPayload,
		  ProjectionExpression: "questionData"
		}
	  }
	};

	// calling dynamoDb
	ddb.batchGetItem(params, function(err, data) {
		if (err) {
			console.log("Error", err);
		} else {
			console.log("GetItem succeeded:", JSON.stringify(data, null, 2));  
		}
	});
});

